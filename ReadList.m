%Read Event List from ModelSim
% Use File > Write List > Events other formats truncate large time values.

function out = ReadList(filename)

fid = fopen(filename,'r');

%Read some data, try to recognize headers, number of signals

data = fread(fid,1000,'*char')';

start = strfind(data,'@');

fseek(fid,start(2)-1,'bof');

%@0 +0
%signal1 x
%signal2 x
%@time +1
%signal1 ######
%@time +1
%signal2 ######

%                   time  delta?      signal value
A = textscan(fid,'@%d64 +%d\r\n/TestBench/%s %d\r\n');

%Identify all signals
signalCount = 1;
out.signals(signalCount).name = A{1,3}{1};

matchingSignals = strcmp(out.signals(1).name,A{1,3});

%Look for additional signals, done when all signals have been matched
while(min(matchingSignals) ~= 1)
    signalCount = signalCount + 1;
        
    out.signals(signalCount).name = A{1,3}{find(matchingSignals == 0,1,'first')};
    
    matchingSignals = matchingSignals | strcmp(out.signals(signalCount).name,A{1,3});
end

for i = 1:length(out.signals)
    signalIndex = strcmp(A{1,3},out.signals(i).name);
    out.signals(i).data = A{1,4}(signalIndex);
    out.signals(i).time = A{1,1}(signalIndex);
end
