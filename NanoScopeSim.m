%OV06946 Analog Output Simulator

%%input image
% Input = imread('puppy.jpg');
% Input = imcrop(Input,[1 1 407 403]);
Input = imread('Pig_Laparoscopy.jpg');
Input = imresize(Input,.25);
Input = imcrop(Input,[100 100 407 403]);


[bayer_out,bayer,hl_data] = RGB2Bayer(Input);

% hl_data = ones(808,204); %black image

%timing information
%Tc
dummy_timing = [11 6 17 6 14 6 20 6];
color_timing = [6 11 6 17];
VBLANK = 30;
FRAME_START = 92;

Td_length = 204;
Tend_length = 8;

Td = ones(1,Td_length);
Td = Td.*1.5;

Tend = ones(1,8);

sync_out = [];
out = [];


% load('fullframe.mat');

%V_blank (1 cycle spi + 25 cycles each (104 lines))
%Frame_Start (4 lines)
% for i = 1:108
%     temp = I(812+i,:);
%     sync_out = [sync_out temp];
% end

% sync_out_scaled = (((sync_out-min(sync_out))/(max(sync_out)-min(sync_out))).*2.7)+.3;


for i = 1:26
    vblank_out = [ones(1,VBLANK).*0.3 ones(1,288*4-VBLANK)];
    out = [out vblank_out];
end

framestart_out = [ones(1,FRAME_START).*0.3 ones(1,288*4-FRAME_START)];
out = [out framestart_out];

%dummy lines (8 lines)
for i = 1:8
    temp = [ones(1,dummy_timing(i)).*0.3 ones(1,76-dummy_timing(i)) ones(1,Td_length) Tend];
    out = [out temp];
end

%image lines (808 half lines)
%need to source from acutual image Td = bayer image of size 288 by 
for i = 1:808
    j = mod(i,4)+1;
    x = [ones(1,color_timing(j)).*0.3 ones(1,76-color_timing(j)) hl_data(i,:) Tend];
    out = [out x];
end

% out = [sync_out_scaled out];

% figure; plot(x);
figure; plot(out);

y = out/max(out);
% y = out;

save output.txt y -ASCII;


fileID = fopen('test.txt','w');
w = waitbar(0,'Saving Signal : ');
for i = 1:length(y)
% for i = 1:10
    fprintf(fileID,sprintf('%s\n', num2str(y(i), '%.3g')));
%     fprintf(fileID,sprintf('%s\n', y(i)));
    waitbar(i/length(y),w,sprintf('Saving Signal...'))
end
delete(w)
fclose(fileID);


% fileID = fopen('test.txt','w');
% s = [];
% for i = 1:length(y)
%     s = [s sprintf('%s\n', num2str(y(i)))];
% end
% fprintf(fileID,s);
% fclose(fileID);