%convert RGB to bayer pattern
%BG
%GR

%%% test input
% y = reshape([1,2,3],1,1,3);
% x = ones(5,5,3);
% I = x.*y;
%%%

%%input image
% I = imread('puppy.jpg');
% I = imcrop(I,[1 1 407 403]);

function [out,bayer,hl_data] = RGB2Bayer(I)
[m n z]= size(I);
out = zeros(m,n);

R = [0,0;0,1];
R = repmat(R,round(m/2),round(n/2));
R = R(1:m,1:n);
G = [0,1;1,0];
G = repmat(G,round(m/2),round(n/2));
G = G(1:m,1:n);
B = [1,0;0,0];
B = repmat(B,round(m/2),round(n/2));
B = B(1:m,1:n);

bayer = zeros(m,n,z);
bayer(:,:,1) = R;
bayer(:,:,2) = G;
bayer(:,:,3) = B;

out = uint8(sum(double(I).*bayer,3));
% figure; imshow(I);
% figure; imshow(out);

hl_data = [];

for k = 1:m
    hl_data = [hl_data; out(k,1:2:end); out(k,2:2:end)];
end

hl_data = ((double(hl_data)./255)*.8)+1;

end