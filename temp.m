%Sync Threshold 
syncThresh = 1000;
syncLength = zeros(1,size(I,1));
syncStart = zeros(1,size(I,1));
syncEnd = zeros(1,size(I,1));
syncMax = zeros(1,size(I,1));
syncMin = zeros(1,size(I,1));
syncAvg = zeros(1,size(I,1));
for i = 1:size(I,1)
    syncs = find(I(i,1:27)<syncThresh);
    if(~isempty(syncs))
        syncStart(i) = syncs(1);
        syncEnd(i) = syncs(end);
        syncMax(i) = max(I(i,syncs));
        syncMin(i) = min(I(i,syncs));
        syncAvg(i) = mean(I(i,syncs));
        syncLength(i) = length(syncs);
    else
        syncStart(i) = -1;
        syncEnd(i) = -1;
        syncMax(i) = -1;
        syncMin(i) = -1;
        syncAvg(i) = -1;
        syncLength(i) = -1;
    end
end