% Only reads optical black and syncs.

global CCUAddress;
CCUAddress = 'COM5';

addpath('SSH&Misc');
S0 = serial('COM5','BaudRate',9600);
fopen(S0);
FCORegWrite(1,15+4*256,'00000001',S0); %Switch to baudrate 115200
fclose(S0);

S1 = serial('COM5','BaudRate',115200);
fopen(S1);


I = [];

TotalLines = 844;

fullLine = 291;
justSync = 27;

BlankLine = cell(1,288);for i = 1:291, BlankLine{i} = [0;0;0;0];end

LineStops = [fullLine*ones(1,20) justSync*ones(1,TotalLines-20)];
    w = waitbar(0,'Loading Sensor Data : ');
% for i = line_start+1:28:line_stop-mod(line_stop-line_start,28) %picture every line is on its own 4
for i = 1:TotalLines %
    fprintf('line = %d\n',i);
    Reg = 219 +(4*256); %Sensor data
%     
%     FCORegWrite(1,218+4*256,'00000010',S1); %disable FIFO
%     pause(1)
%     FCORegWrite(1,218+4*256,sprintf('0%03s0011', dec2hex(i)),S1); %Enable FIFO
%     
    FCORegWrite(1,218+4*256,'00010000',S1); %disable FIFO
    pause(1)
    FCORegWrite(1,218+4*256,sprintf('0%03s0001', dec2hex(i)),S1); %Enable FIFO
    
    code = [80; 96; 72];
    fpga = 1;
    Bank = floor(Reg/256);
    Register = mod(Reg,256);
    send = [code(fpga) + Bank; Register];
    
    %read the button data from FIFO buffer
    if S1.BytesAvailable
        fread(S1.BytesAvailable)
    end
    

    waitbar(i/TotalLines,w,sprintf('Downloading Line: %d of %d',i,TotalLines))
    A = cell(1,291);
    for j = 1:LineStops(i)
        fwrite(S1,send);
        A{j} = fread(S1,4);
    end

    
    %complete the lines with BlankLine
    A(LineStops(i)+1:end) = BlankLine(LineStops(i)+1:end);
    
    %parse data
    for j = 1:length(A)
        Q{j} = [dec2hex(A{j}(1),2) dec2hex(A{j}(2),2) dec2hex(A{j}(3),2) dec2hex(A{j}(4),2)];
    end
    
    dec = hex2dec(Q);
    dec_mat = reshape(dec(4:291),[288,1])';
    I = [I;dec_mat];
    
    % figure; imshow((dec_mat./(2^14)))
end
    delete(w)
FCORegWrite(1,15+4*256,'00000000',S1); %Switch to baudrate 115200
fclose(S1);

[m n] = size(I);
out = zeros(m/2,n*2);
for k = 1:2:m-1
    r1 = I(k,:);
    r2 = I(k+1,:);
    out((k+1)/2,1:2:n*2) = r1;
    out((k+1)/2,2:2:n*2) = r2;
end

figure; imshow(I./(2^14));
figure; imshow(out./(2^14));


%Sync Threshold 
syncThresh = 1000;
syncLength = zeros(1,size(I,1));
syncStart = zeros(1,size(I,1));
syncEnd = zeros(1,size(I,1));
syncMax = zeros(1,size(I,1));
syncMin = zeros(1,size(I,1));
syncAvg = zeros(1,size(I,1));
for i = 1:size(I,1)
    syncs = find(I(i,1:27)<syncThresh);
    if(~isempty(syncs))
        syncStart = syncs(1);
        syncEnd = syncs(end);
        syncMax = max(I(i,syncs));
        syncMin = min(I(i,syncs));
        syncAvg = mean(I(i,syncs));
        syncLength(i) = length(syncs);
    else
        syncStart(i) = -1;
        syncEnd(i) = -1;
        syncMax(i) = -1;
        syncMin(i) = -1;
        syncAvg(i) = -1;
        syncLength(i) = -1;
    end
end
