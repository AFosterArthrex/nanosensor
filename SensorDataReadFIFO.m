% function Q = SensorDataReadFIFO(S1)

global CCUAddress;
CCUAddress = 'COM5';

addpath('SSH&Misc');
S0 = serial('COM5','BaudRate',9600);
fopen(S0);
FCORegWrite(1,15+4*256,'00000001',S0); %Switch to baudrate 115200
fclose(S0);

S1 = serial('COM5','BaudRate',115200);
fopen(S1);


I = [];

pix_line_start = 0;
pix_line_stop = 816;
line_start = 816;
line_stop = 845;

loopstart = [pix_line_start+1:28:pix_line_stop-mod(pix_line_stop-pix_line_start,28) line_start+1:7:line_stop-mod(line_stop-line_start,7)];

% for i = line_start+1:28:line_stop-mod(line_stop-line_start,28) %picture every line is on its own 4
for i = length(loopstart) %sync every 4 lines counts as 1 line
    fprintf('loop = %i\n',loopstart(i));
    Reg = 219 +(4*256); %Sensor data
%     
%     FCORegWrite(1,218+4*256,'00000010',S1); %disable FIFO
%     pause(1)
%     FCORegWrite(1,218+4*256,sprintf('0%03s0011', dec2hex(i)),S1); %Enable FIFO
%     
    FCORegWrite(1,218+4*256,'00010000',S1); %disable FIFO
    pause(1)
    FCORegWrite(1,218+4*256,sprintf('0%03s0001', dec2hex(i)),S1); %Enable FIFO
    
    code = [80; 96; 72];
    fpga = 1;
    Bank = floor(Reg/256);
    Register = mod(Reg,256);
    send = [code(fpga) + Bank; Register];
    
    %read the button data from FIFO buffer
    if S1.BytesAvailable
        fread(S1.BytesAvailable)
    end
    
    w = waitbar(0,'Loading Sensor Data : ');
    A = cell(1,8192);
    for j = 1:8192
        fwrite(S1,send);
        A{j} = fread(S1,4);
        waitbar(j/8192,w,sprintf('Downloading Sensor Data: %d/8192',j))
    end
    delete(w)
    
    %parse data
    for j = 1:length(A)
        Q{j} = [dec2hex(A{j}(1),2) dec2hex(A{j}(2),2) dec2hex(A{j}(3),2) dec2hex(A{j}(4),2)];
    end
    
    dec = hex2dec(Q);
    dec_mat = reshape(dec(4:8067),[288,28])';
    I = [I;dec_mat];
    
    % figure; imshow((dec_mat./(2^14)))
end

FCORegWrite(1,15+4*256,'00000000',S1); %Switch to baudrate 115200
fclose(S1);

[m n] = size(I);
out = zeros(m/2,n*2);
for k = 1:2:m-1
    r1 = I(k,:);
    r2 = I(k+1,:);
    out((k+1)/2,1:2:n*2) = r1;
    out((k+1)/2,2:2:n*2) = r2;
end

figure; imshow(I./(2^14));
figure; imshow(out./(2^14));



% for i = 1:length(A)
%     B{i} = fliplr(mischex2bin(Q{i}));
%     V(i,1) = bin2dec(B{i}(29)); %Button Pressed
%     V(i,2) = bin2dec(fliplr(B{i}(17:27))); %Rest Voltage
%     V(i,3) = bin2dec(fliplr(B{i}(1:11))); %Button Voltage
% end

% out{b} = V;
% figure; plot(out{b}(:,2)); hold on; plot(out{b}(:,3)); title(sprintf('%s %s Button Data',handles.SerialNumber,buttonID{b}))
% xlabel('samples (60 samples/sec)'); ylabel('Button Voltage');
% PlotSaveLocation = sprintf('Button Plots\\%s_%s_%s.png',handles.SerialNumber,buttonID{b},timestr);
% saveas(gcf,PlotSaveLocation)

% DataSaveLocation = sprintf('Button Logs\\%s_%s_%s.csv',handles.SerialNumber,buttonID{b},timestr);
% csvwrite(DataSaveLocation,out{b})
