function [out] = FCOVersionInfo()
% out{1} Processor Version
% out{2} Head Version
% out{3} Isolation Version



%Read Formatter Registers
procregs = [0];
proc = FCORegRead(1,procregs);

%Read Proccesor Registers
headregs = [0];
head = FCORegRead(2,headregs);

%Read Isolation Registers
isoregs = [0];
iso = FCORegRead(3,isoregs);

list = char(65:90);

%Processor Version
ProcV = (proc(procregs == 0,:));
ProcVs = sprintf('850-%.4d-%.2d',hex2dec(ProcV(1:2)),hex2dec(ProcV(3:4)));
if(hex2dec(ProcV(5:6))>25)
    ProcVs = [ProcVs list(floor(hex2dec(ProcV(5:6))/26)) list(mod(hex2dec(ProcV(5:6))+1,26))];
else
    ProcVs = [ProcVs list(mod(hex2dec(ProcV(5:6)),26)+1) ];
end

if(hex2dec(ProcV(5:6)) == 23)  % X number
    ProcVs = [ProcVs num2str(hex2dec(ProcV(7:8)))];
end

out{1} = ProcVs;

%Head Version

HeadV = (head(headregs == 0,:));
HeadVs = sprintf('850-%.4d-%.2d',hex2dec(HeadV(1:2)),hex2dec(HeadV(3:4)));
if(hex2dec(HeadV(5:6))>25)
    HeadVs = [HeadVs list(floor(hex2dec(HeadV(5:6))/26)) list(mod(hex2dec(HeadV(5:6))+1,26)) ];
else
    HeadVs = [HeadVs list(mod(hex2dec(HeadV(5:6)),26)+1) ];
end

if(hex2dec(HeadV(5:6)) == 23)
    HeadVs = [HeadVs num2str(hex2dec(HeadV(7:8)))];
end

out{3} = HeadVs;

IsoV = iso;
IsoVs = sprintf('850-%.4d-%.2d',hex2dec(IsoV(1:2)),hex2dec(IsoV(3:4)));
if(hex2dec(IsoV(5:6))>25)
    IsoVs = [IsoVs list(floor(hex2dec(IsoV(5:6))/26)) list(mod(hex2dec(IsoV(5:6))+1,26))];
else
    IsoVs = [IsoVs list(mod(hex2dec(IsoV(5:6)),26)+1) ];
end

if(hex2dec(IsoV(5:6))==23)
    IsoVs = [IsoVs num2str(hex2dec(IsoV(7:8)))];
end

out{2} = IsoVs;

end