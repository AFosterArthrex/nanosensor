%Grabs the newest raw image from the CCU
% IP = '10.101.45.60';
% IP = '192.168.1.2';

ID = fopen('command.txt','w');

fprintf(ID,'cd /tmp/biooptico\n');
fprintf(ID,'ls\n');

fclose(ID);
 
[status,r] = system(sprintf(...
'plink arthrex@%s -pw Arthrex1 -m command.txt',IP));

for q = 1:round(length(r)/16)
    files(q,:) = r(16*(q-1)+1:16*(q)-1);
end

done = 0;
file = 1;

while(~done)
    if(strcmpi(files(end+1-file,end-2:end),'raw'))
        done = 1;
    else
        file = file + 1;
    end
end

fprintf('Newest raw on CCU is %s\n',files(end+1-file,:))

if(exist(['savedpics/' files(end+1-file,1:end-4) '.png'],'file'))
    fprintf('Reading Saved Image: %s\n',[files(end+1-file,1:end-4) '.png']);
    I = imread(['savedpics/' files(end+1-file,1:end-4) '.png']);
else
    fprintf('Reading Raw Image: %s\n',files(end+1-file,1:end));
    [status,r] = system(sprintf('pscp -pw Arthrex1 arthrex@%s:%s %s\n',IP,['/tmp/biooptico/' files(end+1-file,1:end)],['savedpics/' files(end+1-file,1:end)]));
    I = convertraw(['savedpics/' files(end+1-file,1:end)])/16;
    imwrite(I,['savedpics/' files(end+1-file,1:end-4) '.png'],'png')
    delete(['savedpics/' files(end+1-file,1:end)])
end

% figure;imagesc(I*16)
