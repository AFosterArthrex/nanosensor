function [out r] = SSHFirstConnection(IP)
%Connect for the first time to CCU at IP and save key to the registry

ID = fopen('command.txt','w');
fprintf(ID,'exit');
fclose(ID);
 
[status,r] = system(sprintf(...
'echo y | plink root@%s -pw "" -m command.txt',IP));

out = ~status;