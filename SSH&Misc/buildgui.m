% addpath('3rd Party Programs\','Figures_Guis\','ReportingFunctions\','sfrmat3_post\','SSH&Misc\','Test Functions\','Test Functions\dep','Test Functions\Extras','Test Functions\Scripts')

datetimetag = datestr(now,'yyyymmddHHMMSS');
% 
% uiwait(msgbox(sprintf('Achiving Source Files. Creating Folder: %s\nPress OK to begin',datetimetag)));
% 
% mkdir('Source Backup',datetimetag)
% 
% copyfile('Figures_Guis\',['Source Backup\' datetimetag '\Figures_Guis'])
% copyfile('ReportingFunctions\',['Source Backup\' datetimetag '\ReportingFunctions\'])
% copyfile('SSH&Misc\',['Source Backup\' datetimetag '\SSH&Misc\'])
% copyfile('Test Functions\',['Source Backup\' datetimetag '\Test Functions\'])
% copyfile('Settings',['Source Backup\' datetimetag '\Settings'])
% copyfile('*.m',['Source Backup\' datetimetag])
% %copyfile('*.fig',['Source Backup\' datetimetag])
% copyfile('*.txt',['Source Backup\' datetimetag])

SetupPath
% list = dir(['Source Backup\' datetimetag ]);
% 
% zip(['Source Backup/' datetimetag '.zip'],['*'],['Source Backup/' datetimetag '/'])
% if(exist(['Source Backup/' datetimetag '.zip'],'file'))
%     rmdir(['Source Backup\' datetimetag ],'s')
% else
%     uiwait('zip file not found')
% end

uiwait(msgbox(sprintf('Creating: OpticalTestStation_%s.exe\nPress OK to begin',datetimetag)))

mcc('-m', '-R', '-logfile', '-R', 'log.txt', '-v', 'setupFCO.m', '-a', '*.m', '-a','./3rd Party Programs', ...
    '-a','./Figures_Guis', '-a','./ReportingFunctions', '-a','./sfrmat3_post',...
    '-a','./SSH&Misc', '-a','./Test Functions','-o', sprintf('OpticalTestStation_%s',datetimetag),...
    '-a','C:\MATLAB\SupportPackages\R2015a\osgenericvideointerface',...
    '-a','C:\Program Files\MATLAB\R2015a\java\jar\toolbox');

movefile([sprintf('OpticalTestStation_%s',datetimetag) '.exe'],['C:\FCO executables\' sprintf('OpticalTestStation_%s',datetimetag)  '.exe'])

fprintf('Writing Note in Change Log...')

fprintf('Done\n')