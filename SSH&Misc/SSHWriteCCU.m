 function [status result] = SSHWriteCCU(IP,fpga,addr,value)

string = 'gpmc_write /dev/gpmc_cs';

ID = fopen('command.txt','w');

for i = 1:length(addr)
    fprintf(ID,'%s%d %d 0x%s\n',string,fpga,addr(i),value(i,:));
end
% fprintf(ID,'exit');
fclose(ID);
 
[status,result] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

end