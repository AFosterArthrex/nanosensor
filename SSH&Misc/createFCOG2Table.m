fid = fopen('databasesettings.txt');
temp = textscan(fid,'%s %[^\r\n]');
dbsettings = cell2struct(temp{2},temp{1});
dbseverstring = sprintf('jdbc:sqlserver://%s:%s;databaseName=%s',dbsettings.Servername,dbsettings.Port,dbsettings.Databasename);
c = database(dbseverstring,dbsettings.Username,dbsettings.Password,'com.microsoft.sqlserver.jdbc.SQLServerDriver');

s = jdbcexecute(c,sprintf('CREATE TABLE [dbo].[TESTHeadFCO] ADD %s %s NULL;',DBOutput{1,i},type));

s = jdbcexecute(c,sprintf('ALTER TABLE [dbo].[TESTHeadFCO] ADD PartNumber %s NULL;',DBOutput{1,i},type));
s = jdbcexecute(c,sprintf('ALTER TABLE [dbo].[TESTHeadFCO] ADD TestDate %s NULL;',DBOutput{1,i},type));
s = jdbcexecute(c,sprintf('ALTER TABLE [dbo].[TESTHeadFCO] ADD TestTime %s NULL;',DBOutput{1,i},type));


for i = 5:size(DBOutput,2)
    
    switch class(DBOutput{2,i})
        case 'logical'
            type = 'bit';
        case 'char'
            type = 'varchar(255)';
        case 'double'
            type = 'float';
        otherwise
            type = 'varchar(255)';
    end
    s = jdbcexecute(c,sprintf('ALTER TABLE [dbo].[TESTHeadFCO] ADD %s %s NULL;',DBOutput{1,i},type));
end