function [dat MTF50 MTF10 MTF5 MTF2] = wrapsfrmat3(I,ROIs,z)
%wrapper for slant edge test

%I = image
%ROIs = region of interest vector [topleftcorner_x,topleftcorner_y,sizex,sizey]

%MTF50 = 50% contrast frequency (Cycles per Pixel)

n = size(ROIs,1);  %number of regions
MTF50 = zeros(n,4);
MTF10 = zeros(n,4);
MTF5 = zeros(n,4);
MTF2 = zeros(n,4);
for j = 1:n
    
    %crop image
    if(z) %     Crop image with top left corner and size ROI
    Ic{j} = I(ROIs(j,2):ROIs(j,2)+ROIs(j,4)-1,ROIs(j,1):ROIs(j,1)+ROIs(j,3)-1,:);

    else %     Crop image with top corner and lower right
    Ic{j} = I(ROIs(j,2):ROIs(j,4),ROIs(j,1):ROIs(j,3),:);
    end
%     
%     imagesc(Ic{j})
%     pause
    [status, dat, ~, ~, ~, ~, ~] = sfrmat3(1,1,[0.213   0.715   0.072],Ic{j});
    if(status)
        MTF50(j,:) = [0 0 0 0];
        continue
    end
    %FindMTF50 for each channel
    for i = 1:4
        q = find(dat(:,i+1) <= 0.5,1);
        if(isempty(q))
            q = size(dat,1);
        end
        Ly = dat(q,i+1);
        Lx = dat(q,1);
        Hy = dat(q-1,i+1);
        Hx = dat(q-1,1);
        
        MTF50(j,i) = Hx- (Hx-Lx)/(Hy-Ly)*(Hy-0.5);
    end
    %FindMTF10 for each channel
    for i = 1:4
        q = find(dat(:,i+1) <= 0.1,1);
        if(isempty(q))
            q = size(dat,1);
        end
        Ly = dat(q,i+1);
        Lx = dat(q,1);
        Hy = dat(q-1,i+1);
        Hx = dat(q-1,1);
        
        MTF10(j,i) = Hx- (Hx-Lx)/(Hy-Ly)*(Hy-0.1);
    end
    %FindMTF5 for each channel
    for i = 1:4
        q = find(dat(:,i+1) <= 0.05,1);
        if(isempty(q))
            q = size(dat,1);
        end
        Ly = dat(q,i+1);
        Lx = dat(q,1);
        Hy = dat(q-1,i+1);
        Hx = dat(q-1,1);
        
        MTF5(j,i) = Hx- (Hx-Lx)/(Hy-Ly)*(Hy-0.05);
    end
    %FindMTF2 for each channel
    for i = 1:4
        q = find(dat(:,i+1) <= 0.02,1);
        if(isempty(q))
            q = size(dat,1);
        end
        Ly = dat(q,i+1);
        Lx = dat(q,1);
        Hy = dat(q-1,i+1);
        Hx = dat(q-1,1);
        
        MTF2(j,i) = Hx- (Hx-Lx)/(Hy-Ly)*(Hy-0.02);
    end
end
end
% plot(dat(:,1), dat(:,3),'k') 
% axis tight
% hold on
% plot(MTF50(2),0.5,'*')