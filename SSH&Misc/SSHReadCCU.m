 function [status result] = SSHReadCCU(IP,fpga,addr)

if(length(addr) > 32)
    [s result1]= SSHReadCCU(IP,fpga,addr(1:32));
    [s result2]= SSHReadCCU(IP,fpga,addr(33:end));
    
    result = [result1;result2];
    status = 0;
else

%1 Processor
%2 Formatter
%3 ISO (Use fpga 2 addr+512)
if(fpga == 3)
    fpga = 2;
    addr = addr + 512;
end

string = 'gpmc_read /dev/gpmc_cs';

ID = fopen('command.txt','w');

for i = 1:length(addr)
    fprintf(ID,'%s%d %d\n',string,fpga,addr(i));
end
fprintf(ID,'exit');
fclose(ID);
 
[status,r] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

for i = 1:length(addr)
    result(i,:) = r(11+19*(i-1):11+19*(i-1)+7);
end
end
% 
% if(~status)
%     out = result(end-8:end-1);
% else
%     out = 'error';
% end

end