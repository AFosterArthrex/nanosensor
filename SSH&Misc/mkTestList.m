function TestList = mkTestList(filename)
%Create TestList array from TestList.xlsx

[num, txt, raw] = xlsread(filename);

%Find Number and location of nonSubTests
TestIndx = find(~strcmpi('SUB',txt(2:end,1)))+1;
TestNum = length(TestIndx)-2;

%TestList
%Rows are Tests
%Columns are: 
%1 - Test Code,
%2 - Test Name, 
%3 - Test Criteria (array, #Rows = #Subtests+1summary, #Columns = 4 [Name Min Max Nom])
%4 - Results (array, #Rows = #Subtests+1summary, #Columns = 3 [Name Pass/Fail Data])
%5 - Raw data (any extra data from test, no standard format)
TestList = cell(TestNum,5);

for i = 1:length(TestIndx)
    TestList{i,1} = raw{TestIndx(i),1};
    TestList{i,2} = raw{TestIndx(i),2};
    if(str2double(raw{TestIndx(i),3}) == 0)
        TestList{i,3} = {raw{TestIndx(i),2} raw{TestIndx(i),4} raw{TestIndx(i),5} raw{TestIndx(i),6}};
        TestList{i,4} = cell(1,3);
        TestList{i,4}{1,1} = raw{TestIndx(i),2};
        TestList{i,6}{1} = '';
    else
        tmparray = cell(str2double(raw{TestIndx(i),3}),4);
        comments = cell(str2double(raw{TestIndx(i),3}),1);
        for j = 1:str2double(raw{TestIndx(i),3})
            tmparray{j,1} = [raw{TestIndx(i),2} ' - ' raw{TestIndx(i)+j,2}];     %Name
            tmparray{j,2} = raw{TestIndx(i)+j,4};     %Max
            tmparray{j,3} = raw{TestIndx(i)+j,5};     %Min
            tmparray{j,4} = raw{TestIndx(i)+j,6};     %Nom
            if(~isnan(raw{TestIndx(i)+j,7}))
                comments{j} =   raw{TestIndx(i)+j,7};     %Comment
            else
                comments{j} =   '';     %No Comment
            end
        end
        
        
        
        TestList{i,3} = tmparray;
        TestList{i,4} = cell(str2double(raw{TestIndx(i),3}),3);
        TestList{i,4}(:,1) = tmparray(:,1);
        TestList{i,6} = comments;
    end
end