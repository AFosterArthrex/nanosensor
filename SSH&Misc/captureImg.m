uiwait(msgbox('press left button to capture'))
IP = handles.settings.IPaddress;

ID = fopen('command.txt','w');

fprintf(ID,'cd /tmp/biooptico\n');
fprintf(ID,'ls\n');

fclose(ID);
 
[status,r] = system(sprintf(...
'plink arthrex@%s -pw Arthrex1 -m command.txt',IP))

for q = 1:round(length(r)/16)
    files{q} = r(16*(q-1)+1:16*(q)-1);
end

[B, IX] = sort(files)

ID = fopen('command.txt','w');

s = what;

fclose(ID);
 
[status,r] = system(sprintf('pscp -pw Arthrex1 arthrex@%s:%s %s\n',IP,['/tmp/biooptico/' files{IX(end)}],[files{IX(end)}]))


I = convertraw(sprintf('%s\\%s',s.path,files{IX(end)}));

imwrite(I,['BioOptico Capture ' datestr(now) '.png'],'png')

figure;imagesc(I)