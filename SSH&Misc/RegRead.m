function [ out ] = RegRead( Address, FPGA, Reg, varargin )
%REGWRITE Reads 32-bit Binary Data from Register
%   Reads from FPGA register using GPMC (Ethernet) or
%   RS232 (Serial)
%
%   Inputs:
%   1. Address - Serial Port (Serial Mode) or IP address (Ethernet)
%   2. FPGA - 1 = Formatter, 2 = Processor, 3 = Isolation
%   3. Reg - Target Register

if(length(Address) > 3 && strcmpi(Address(1:3),'COM'))
    %Serial Mode
    answer = 'Retry';
    while(strcmpi(answer,'Retry') || strcmpi(answer,'Close All'))
        try
            out = SerialReadCCU(Address,FPGA,Reg,varargin{:});
            answer = 'Done';
        catch err
            out = '';
            getReport(err)
            if(strcmpi(err.identifier,'MATLAB:serial:fopen:opfailed'))
                answer = questdlg(sprintf('Cannot Open: %s.',Address),'Serial Communication Error','Retry','Close All','Cancel','Retry');
            end
            if(strcmpi('Close All',answer))
                ports = instrfindall
                fclose(ports(strcmpi(ports.Name,['Serial-' Port])))
                delete(ports(strcmpi(ports.Name,['Serial-' Port])))
            end
        end
    end

else
    %Ethernet Mode
    [~, out] = SSHReadCCU(Address,FPGA,Reg);
end

