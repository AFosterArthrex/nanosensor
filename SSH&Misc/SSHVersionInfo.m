function [out] = SSHVersionInfo(IP)
% out{1} Formatter Version
% out{2} Processor Version
% out{3} Head CLPD Version
% out{4} Isolation Version
% out{5} Video In  Version
% out{6} Software  Version



%Read Formatter Registers
formregs = [0];
[s form] = SSHReadCCU(IP,1,formregs);

%Read Proccesor Registers
procregs = [0 512];
[s proc] = SSHReadCCU(IP,2,procregs);

list = char(65:90);

%Processor Version
ProcV = (proc(procregs == 0,:));
ProcVs = sprintf('850-%.4d-00',hex2dec(ProcV(1:2)));
if(hex2dec(ProcV(5:6))>25)
    ProcVs = [ProcVs list(floor(hex2dec(ProcV(5:6))/26)) list(mod(hex2dec(ProcV(5:6))+1,26))];
else
    ProcVs = [ProcVs list(mod(hex2dec(ProcV(5:6)),26)+1)];
end
out{1} = ProcVs;

%Formatter Version

FormV = (form(formregs == 0,:));
FormVs = sprintf('850-%.4d-00',hex2dec(FormV(1:2)));
if(hex2dec(FormV(5:6))>25)
    FormVs = [FormVs list(floor(hex2dec(FormV(5:6))/26)) list(mod(hex2dec(FormV(5:6))+1,26))];
else
    FormVs = [FormVs list(mod(hex2dec(FormV(5:6)),26)+1)];
end
out{2} = FormVs;


%Head CLPD Version Doesnt work needs serial
[s r] = SSHWriteCCU(IP,2,512+9,'d','80090000');
pause(0.1)
[s r] = SSHReadCCU(IP,2,512+9);
clpdV = r(5:8);
[s r] = SSHWriteCCU(IP,2,512+9,'d','80080000');
pause(0.1)
[s r] = SSHReadCCU(IP,2,512+9);
clpdV = [clpdV r(5:6)];
clpdVs = sprintf('850-%.4d-00',hex2dec(clpdV(1:2)));
if(hex2dec(clpdV(5:6))>25)
    clpdVs = [clpdVs list(floor(hex2dec(clpdV(5:6))/26)) list(mod(hex2dec(clpdV(5:6))+1,26))];
else
    clpdVs = [clpdVs list(mod(hex2dec(clpdV(5:6)),26)+1)];
end
out{3} = clpdVs;

IsoV = (proc(procregs == 512,:));
IsoVs = sprintf('850-%.4d-00',hex2dec(IsoV(1:2)));
if(hex2dec(IsoV(5:6))>25)
    IsoVs = [IsoVs list(floor(hex2dec(IsoV(5:6))/26)) list(mod(hex2dec(IsoV(5:6))+1,26))];
else
    IsoVs = [IsoVs list(mod(hex2dec(IsoV(5:6)),26)+1)];
end
out{4} = IsoVs;


VideoV = (proc(procregs == 0,:));


%Software Version
ID = fopen('command.txt','w');

fprintf(ID,'cat /osversion');

% fprintf(ID,'exit');
fclose(ID);
 
[status,result] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

out{6} = result;

%Full Software Version
ID = fopen('command.txt','w');

fprintf(ID,'cat /etc/angstrom-version');

% fprintf(ID,'exit');
fclose(ID);
 
[status,result] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

out{7} = result;


end