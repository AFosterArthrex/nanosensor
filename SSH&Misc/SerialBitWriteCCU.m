function SerialBitWriteCCU(Port,fpga,Reg,Value,Loc,varargin)

if(~isempty(varargin))
    temp = SerialReadCCU(Port,fpga,Reg,varargin{1});
else
    temp = SerialReadCCU(Port,fpga,Reg);
end

b = fliplr(mischex2bin(temp));
for i = 1:length(Loc)
    b(Loc(i)+1) = num2str(Value(i));
end
v = miscbin2hex(fliplr(b));

if(~isempty(varargin))
    SerialWriteCCU(Port,fpga,Reg,v,varargin{1});
else
    SerialWriteCCU(Port,fpga,Reg,v);
end
end