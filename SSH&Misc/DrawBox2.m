
% draws an overlay box
function [Iover] = DrawBox2(Iover,loc,t)
%loc = [x1 x2 y1 y2];

imgSize = size(Iover);

%Cap X
loc(:,[1 2]) = min(imgSize(2)-t,max(t+1,loc(:,[1 2])));
%Cap Y
loc(:,[3 4]) = min(imgSize(1)-t,max(t+1,loc(:,[3 4])));

for i = 1:size(loc,1)

Iover(loc(i,3)+[-t:t],loc(i,1)-t:loc(i,2)+t) = 1;
Iover(loc(i,4)+[-t:t],loc(i,1)-t:loc(i,2)+t) = 1;
Iover(loc(i,3)-t:loc(i,4)+t,loc(i,1)+[-t:t]) = 1;
Iover(loc(i,3)-t:loc(i,4)+t,loc(i,2)+[-t:t]) = 1;

% Iover(loc(i,3)-1,loc(i,1):loc(i,2)) = 1;
% Iover(loc(i,4)-1,loc(i,1):loc(i,2)) = 1;
% Iover(loc(i,3):loc(i,4),loc(i,1)-1) = 1;
% Iover(loc(i,3):loc(i,4),loc(i,2)-1) = 1;
% 
% Iover(loc(i,3)+1,loc(i,1):loc(i,2)) = 1;
% Iover(loc(i,4)+1,loc(i,1):loc(i,2)) = 1;
% Iover(loc(i,3):loc(i,4),loc(i,1)+1) = 1;
% Iover(loc(i,3):loc(i,4),loc(i,2)+1) = 1;
end

end