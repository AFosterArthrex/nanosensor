function [out] = SSHReadAllRegs(IP)
%SSHReadAllRegs - Read all registers (processor and formatter)
%Inputs:
%   IP - IP address of CCU
%Outputs:
%   out - results, 8x64x3

%Read Formatter Registers
formregs = [0:63];
[s form] = SSHReadCCU(IP,1,formregs);

%Read Proccesor Registers
procregs = [0:63];
[s proc] = SSHReadCCU(IP,2,procregs);

%Read Isolation Registers
isoregs = [0:63];
[s iso] = SSHReadCCU(IP,2,512+isoregs);
% 
out = cat(3,form,proc,iso);

