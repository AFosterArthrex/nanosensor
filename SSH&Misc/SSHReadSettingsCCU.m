function [settings] = SSHReadSettingsCCU(IP)
% I - input image (index, 1 = HRamp 2 = VRamp 3 = PRandom)
% loc - Test Pattern Location (index, 0 = Processor Input 1 = Formatter
% Input 2 = Vidwr Input 3 = Vidrd Input 4 = Zoom Input)
% Over - overlay image (index, 0 = HRamp 1 = VRamp 2 = PRandom)
% aec - AEC gain (1 to turn off)
% pix - pixel replace (0 to turn off)
% rgb - white balance rgb gains (1 to turn off)
% sharp - sharpness gain (0 to turn off)
% S - scaling factor (scaler) (use 2 for normal operation, 1 to turn off)
% G - green offset (use 1 for normal operation, 0 to turn off)
% P - peaking gain (0 to turn off)
% Z - scaling index (zoom) (0-40, 0 to turn off)
% A - overlay alpha (0 to turn overlay off)
% IP - IP address of CCU

I = 0;
Over = 0;
loc = -2;

%Read Formatter Registers
formregs = [0 8 9 14 25 26 34];
[s form] = SSHReadCCU(IP,1,formregs);

%Read Proccesor Registers
procregs = [0 8 10 11 12 13 14 15 16 17 28 29 32];
[s proc] = SSHReadCCU(IP,2,procregs);

% Test Pattern

%Check Unpack module
if(str2double(proc(procregs == 8,6)) > 0)
    loc = -1;
    switch str2double(proc(procregs == 8,6))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Processor Output
if(str2double(proc(procregs == 8,5)) > 0)
    loc = 0;
    switch str2double(proc(procregs == 8,5))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Formatter Input
if(str2double(form(formregs == 8,2)) > 0)
    loc = 1;
    switch str2double(form(formregs == 8,2))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Video Write Input
if(str2double(form(formregs == 8,1)) > 0)
    loc = 2;
    switch str2double(form(formregs == 8,1))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Video Read Input
if(str2double(form(formregs == 8,3)) > 0)
    loc = 3;
    switch str2double(form(formregs == 8,3))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Zoom Input
if(str2double(form(formregs == 9,2)) > 0)
    loc = 4;
    switch str2double(form(formregs == 9,2))
        case 1
            I = 1;
        case 2
            I = 2;
        case 4
            I = 3;
        otherwise
            I = 0;
    end
end

%Check Gui Write Input
if(str2double(form(formregs == 8,4)) > 0)
%     loc = 5;
    switch str2double(form(formregs == 8,4))
        case 1
            Over = 1;
        case 2
            Over = 2;
        case 4
            Over = 3;
        otherwise
            Over = 0;
    end
end

%Check Gui Read Input
if(str2double(form(formregs == 9,3)) > 0)
%     loc = 6;
    switch str2double(form(formregs == 9,3))
        case 1
            Over = 1;
        case 2
            Over = 2;
        case 4
            Over = 3;
        otherwise
            Over = 0;
    end
end

%AEC
aec = hex2dec(proc(procregs == 11,2:4));

%PIX
if(hex2dec(proc(procregs == 32,7)) == 0)
    pix = 1;
else
    pix = 0;
end

%White Balance
r = hex2dec(proc(procregs == 15,5:8));
g = hex2dec(proc(procregs == 16,5:8));
b = hex2dec(proc(procregs == 17,5:8));

rgb = [r g b];

%Sharpening
sharp = hex2dec(proc(procregs == 28,7:8))/2;

%Peaking
peak = hex2dec(proc(procregs == 28,5:6));

S = 2;

%Green Offset
d = mischex2bin(form(formregs == 9,:));
if(str2double(d(31)) == 0)
    G = 1;
else
    G = 0;
end

%Post Peaking
P = hex2dec((form(formregs == 25,6:8)));

%Zoom
d = mischex2bin(form(formregs == 9,:));
Z = bin2dec(d(15:20));

%Alpha
A = 0;

ver = SSHVersionInfo(IP);
FormV = ver{1};
ProcV = ver{2};
HeadV = ver{3};
IsoV = ver{4};
VideoV = ver{5};
SoftV = ver{6};

settings = CCUsettings('Iindex',I,...
                       'Loc',loc,...
                       'Over',Over,...
                       'AEC',aec,...
                       'Pix',pix,...
                       'RGB',rgb,...
                       'Sharp',sharp,...
                       'Peaking',peak,...
                       'Green',G,...
                       'PostPeaking',P,...
                       'Zoom',Z,...
                       'Alpha',A,...
                       'IP',IP,...
                       'FormatterVersion',FormV,...
                       'ProcessorVersion',ProcV,...
                       'HeadCPLDVersion',HeadV,...
                       'IsolationBoard',IsoV,...
                       'VideoInputBoard',VideoV,...
                       'Software',SoftV);
