function [status result] = SSHbitWrite(IP,fpga,addr,loc,value)
% 1 register at a time
% multiple bits at a time

string = 'gpmc_read /dev/gpmc_cs';

ID = fopen('command.txt','w');
fprintf(ID,'%s%d %d\n',string,fpga,addr);
fprintf(ID,'exit');
fclose(ID);
 
[status,result] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

h = result(end-8:end-1);
b = fliplr(mischex2bin(h));
for i = 1:length(loc)
    b(loc(i)+1) = num2str(value(i));
end
v = miscbin2hex(fliplr(b));

string = 'gpmc_write /dev/gpmc_cs';

ID = fopen('command.txt','w');

for i = 1:length(addr)
    fprintf(ID,'%s%d %d 0x%s\n',string,fpga,addr(i),v(i,:));
end
fprintf(ID,'exit');
fclose(ID);
 
[status,result] = system(sprintf(...
'plink root@%s -pw '''' -m command.txt',IP));

end