function out = miscbin2hex(in)

out = dec2hex(bin2dec(in),8);

end