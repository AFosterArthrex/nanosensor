function [ out ] = FCOBitWrite( FPGA, Reg, Loc, Data, varargin )

r = FCORegRead(FPGA,Reg);

b = fliplr(mischex2bin(r));
for i = 1:length(Loc)
    b(Loc(i)+1) = num2str(Data(i));
end
v = miscbin2hex(fliplr(b));

FCORegWrite(FPGA,Reg,v,varargin);

end