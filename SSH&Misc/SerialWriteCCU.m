function SerialWriteCCU(Port,fpga,Reg,Value,varargin)

if(~isempty(varargin) && ~isempty(varargin{1}) && strcmpi(class(varargin{1}),'serial') && varargin{1}.isvalid && strcmpi(varargin{1}.status,'open'))
    S1 = varargin{1};
else
    retry = 0; done = 0; MaxRetry = 5;
    while(~done & retry < MaxRetry)
        retry = retry + 1;
        S1 = serial(Port,'BaudRate',9600);
        try
            fopen(S1);
            done = 1;
        catch err
            getReport(err)
            ports = instrfindall
            try
                fclose(ports(strcmpi(ports.Name,['Serial-' Port])))
            catch
            end
            delete(ports(strcmpi(ports.Name,['Serial-' Port])))
        end
    end
end

code = [144; 160; 136];

Bank = floor(Reg/256);

Reg = mod(Reg,256);

send = [code(fpga)+Bank; Reg; hex2dec(Value(1:2)); hex2dec(Value(3:4)); hex2dec(Value(5:6)); hex2dec(Value(7:8))];

fwrite(S1,send);

if(~(~isempty(varargin) && ~isempty(varargin{1})))
    fclose(S1);
    delete(S1);
end