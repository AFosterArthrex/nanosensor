function settings = ParseSettings(inputfile)

fid = fopen(inputfile);
C = textscan(fid,'%s %s %s\r\n');
fclose(fid);

for i = 2:length(C{1})
    name = C{1}{i};
    value = C{2}{i};
    stringorint = C{3}{i};
    if(strcmpi(stringorint,'i'))
        settings.(name) = str2double(value);
    elseif(strcmpi(stringorint,'a'))
        tmp = textscan(value,'%s',4,'delimiter','.');
        settings.(name) = [str2double(tmp{1}{1}) str2double(tmp{1}{2}) str2double(tmp{1}{3}) str2double(tmp{1}{4})];
    else
        settings.(name) = value;
    end
end