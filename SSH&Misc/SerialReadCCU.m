function out = SerialReadCCU(Port,fpga,Reg,varargin)

if(~isempty(varargin) && ~isempty(varargin{1}) && strcmpi(class(varargin{1}),'serial') && varargin{1}.isvalid && strcmpi(varargin{1}.status,'open'))
    S1 = varargin{1};
else
    retry = 0; done = 0; MaxRetry = 5;
    while(~done & retry < MaxRetry)
        retry = retry + 1;
        S1 = serial(Port,'BaudRate',9600);
        try
            fopen(S1);
            done = 1;
        catch err
            getReport(err)
            ports = instrfindall
            try
                fclose(ports(strcmpi(ports.Name,['Serial-' Port])))
            catch
            end
            delete(ports(strcmpi(ports.Name,['Serial-' Port])))
        end
    end
end

code = [80; 96; 72];

Bank = floor(Reg/256);

Reg = mod(Reg,256);

send = [code(fpga) + Bank; Reg];

fwrite(S1,send);

[A,count,msg] = fread(S1,4);

if(~(~isempty(varargin) && ~isempty(varargin{1})))
    fclose(S1);
    delete(S1);
end

out = [dec2hex(A(1),2) dec2hex(A(2),2) dec2hex(A(3),2) dec2hex(A(4),2)];


