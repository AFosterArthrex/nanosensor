function [ out ] = FCORegWrite( FPGA, Reg, Data, varargin )

global CCUAddress

if(~isempty(CCUAddress))
    out = RegWrite(CCUAddress,FPGA,Reg,Data,varargin{:});
    if(out)
        return
    else
        error('Error Using global variable CCUAddress = %s. Output = %s.',CCUAddress,out);
    end
        
else
    %Use Fallback Address from text file
    fprintf('Global Variable "CCUAddress" not found. Trying "ComFallbackSettings.txt"');
    if(exist('ComFallbackSettings.txt','file'))
        FallbackAddress = importdata('ComFallbackSettings.txt')
        for i = length(FallbackAddress)
            out = RegWrite(FallbackAddress{i},FPGA,Reg,Data,varargin{:});
            if((out))
                break
            else
                %Try The next one
            end
        end
    else
        warndlg(sprintf('ComFallbackSettings.txt not found'))
    end
end         
            
            
            
function out = CheckValue(in)
%should be an eight charecter string of hex values

out = 1;

%Remove 0x if detected
if(strcmpi(in(1:2),'0x'))
    in = in(3:end);
end

if(length(in) ~= 8)
    out = 0;
    warndlg(sprintf('Error in CheckValue(). Expected 8 charecters found: %d',length(in)))
    return
    
elseif(min(double(in)) < 48 || min(double(in)) > 69)
    out = 0;
    warndlg(sprintf('Error in CheckValue(). Expected Hex Value String, found: %s',in))
    return
end