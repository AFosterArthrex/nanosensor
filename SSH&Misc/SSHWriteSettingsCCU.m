function [] = SSHWriteSettingsCCU(settings)
% I - input image (index, 0 = HRamp 1 = VRamp 2 = PRandom)
% loc - Test Pattern Location (index, -1 = Unpack Module 0 = Processor Input 1 = Formatter
% Input 2 = Vidwr Input 3 = Vidrd Input 4 = Zoom Input)
% Over - overlay image (index, 0 = HRamp 1 = VRamp 2 = PRandom)
% aec - AEC gain (1 to turn off)
% pix - pixel replace (0 to turn off)
% rgb - white balance rgb gains (1 to turn off)
% sharp - sharpness gain (0 to turn off)
% S - scaling factor (scaler) (use 2 for normal operation, 1 to turn off)
% G - green offset (use 1 for normal operation, 0 to turn off)
% P - peaking gain (0 to turn off)
% Z - scaling index (zoom) (0-40, 0 to turn off)
% A - overlay alpha (0 to turn overlay off)
% IP - IP address of CCU

% Outputs:
% out - 

%Test Pattern
%First Turn off all patterns
[s r] = SSHbitWrite(settings.IP,2,8,fliplr(8:15),zeros(1,16));
[s r] = SSHbitWrite(settings.IP,1,8,fliplr(16:31),zeros(1,16));
[s r] = SSHbitWrite(settings.IP,1,9,fliplr(20:27),zeros(1,8));


switch settings.Loc
    case -1
        addr = 8;
        loc = [11 10 9 8];
    case 0
        addr = 8;
        loc = [15 14 13 12];
    case 1
        addr = 8;
        loc = [27 26 25 24];
    case 2
        addr = 8;
        loc = [31 30 29 28];
    case 3
        addr = 8;
        loc = [23 22 21 20];
    case 4
        addr = 9;
        loc = [27 26 25 24];
    otherwise
        addr = 0;
        loc = 0;
end
switch settings.Iindex
    case 1
        value = '0001';
    case 2
        value = '0010';
    case 3
        value = '0100';
    otherwise
        value = '0000';
end
if(settings.Loc <= 0)
    [s r] = SSHbitWrite(settings.IP,2,addr,loc,value);
else
    [s r] = SSHbitWrite(settings.IP,1,addr,loc,value);
end


switch settings.Over
    case 1
        value = '0001';
    case 2
        value = '0010';
    case 3
        value = '0100';
    otherwise
        value = '0000';
end
switch settings.OverLoc
    case 1
        [s r] = SSHbitWrite(settings.IP,1,8,[19 18 17 16],value);
    case 2
        [s r] = SSHbitWrite(settings.IP,1,9,[23 22 21 20],value);
    otherwise
end

%Proccessing Options


%Freeze non-static TPGs
[s r] = SSHbitWrite(settings.IP,1,8,[13],1);

%AEC
[s r] = SSHbitWrite(settings.IP,2,10,[2 1],dec2bin(2)); %Manual Gain
[s r] = SSHbitWrite(settings.IP,2,11,[27 26 25 24 23 22 21 20 19 18 17 16],dec2bin(settings.AEC,12));

%Pix
[s r] = SSHbitWrite(settings.IP,2,32,[4],dec2bin(~settings.Pix,1));

%White Balance
[s r] = SSHbitWrite(settings.IP,2,15,[12 11 10 9 8 7 6 5 4 3 2 1 0],dec2bin(settings.RGB(1),13));
[s r] = SSHbitWrite(settings.IP,2,16,[12 11 10 9 8 7 6 5 4 3 2 1 0],dec2bin(settings.RGB(2),13));
[s r] = SSHbitWrite(settings.IP,2,17,[12 11 10 9 8 7 6 5 4 3 2 1 0],dec2bin(settings.RGB(3),13));

%Sharpening
[s r] = SSHbitWrite(settings.IP,2,28,[7 6 5 4 3 2 1],dec2bin(settings.Sharp,7));

%Peaking
[s r] = SSHbitWrite(settings.IP,2,28,[14 13 12 11 10 9 8],dec2bin(settings.Peaking,7));

%Gamma
[s r] = SSHbitWrite(settings.IP,2,8,4,~settings.Gamma);

%Green Offset
[s r] = SSHbitWrite(settings.IP,1,9,1,~settings.Green);

%Zoom
[s r] = SSHbitWrite(settings.IP,1,9,[17 16 15 14 13 12],dec2bin(settings.Zoom,6));

%PostPeaking
[s r] = SSHbitWrite(settings.IP,1,25,[9 8 7 6 5 4 3 2 1 0],dec2bin(settings.PostPeaking,10));
%Turn off peaking attenuation
[s r] = SSHbitWrite(settings.IP,1,25,[12],'1');

%Alpha

%Background
[s r] = SSHbitWrite(settings.IP,1,14,[8 7 6 5 4 3 2 1 0],dec2bin(settings.Alpha,9));

%AlphaWindow

for i = 1:16
    Hex = dec2hex([settings(1).AlphaWindow(i,1)*2^16+settings(1).AlphaWindow(i,2);...
             settings(1).AlphaWindow(i,3)*2^16+settings(1).AlphaWindow(i,4);...
             2^10+settings(1).AlphaWindow(i,5)],8);
    [s r] = SSHWriteCCU(settings.IP,1,(192:194)+(i-1)*3,Hex);
end