function [varargout] = G2Capture(varargin)
%Mode - 1 basic capture
%     - 2 12 bit (centered)
%     - 3 12 bit (full 1080p)
%     - 4 4k from Quad 3g
%     - 5 Quad 3g 1 top left
%     - 6 Quad 3g 2 top right
%     - 7 Quad 3g 3 bottom left
%     - 8 Quad 3g 4 bottom right
%     - 9 4k from Displayport
%     - 10 Displayport Stripe 1
%     - 11 Displayport Stripe 2
%     - 12 Displayport Stripe 3
%     - 13 Displayport Stripe 4
%Vid - Video Capture Object
%Port - Serial Address (COM#)






if(length(varargin) >= 1)
    Mode = varargin{1};
else
    Mode = 1;
end

if(length(varargin) >= 2)
    vid = varargin{2};
else
    vid = videoinput('winvideo');
end

SerialModes = ones(1,20);
SerialModes([1 19]) = 0;

if(SerialModes(Mode))
    if(length(varargin) >= 3)
        Port = varargin{3};
    else
        Port = 'COM12';
    end
    
    if(Mode ~= 1)
        retry = 0; done = 0; MaxRetry = 5;
        while(~done & retry < MaxRetry)
            retry = retry + 1;
            S1 = serial(Port,'BaudRate',9600);
            try
                fopen(S1);
                done = 1;
            catch err
                getReport(err)
                ports = instrfindall
                fclose(ports(strcmpi(ports.Name,['Serial-' Port])))
                delete(ports(strcmpi(ports.Name,['Serial-' Port])))
            end
        end
    end
    

end

switch Mode
    case 1 %Simple Capture no changing registers, no conversion
        %Disable 12-bit mode
%         SerialWriteCCU(Port,1,24,'00000000',S1);
        pause(1)
        Ic = getsnapshot(vid);
        varargout{1} = Ic;
        
    case 2 %12bit 960x1080 capture
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'00000071',S1);
        pause(0.2)
        Ic = getsnapshot(vid);
        varargout{1} = convert8to12(Ic);
           
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'00000000',S1);
                       
    case 3 %12bit 1920x1080 capture
        %View Buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        %1080 from frame buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);

        %Freeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'1',[18],S1);
        pause(0.15)
        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.15)
        Ic = getsnapshot(vid);
        Ic = YUY2toRGB(Ic);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.15)
        Ic = getsnapshot(vid);
        Ic = YUY2toRGB(Ic);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
                
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[18],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
        varargout{1} = [Ileft Iright];
        
    case 4 %8bit 3840x2160 capture
        uiwait(msgbox('Don''t Use This Mode!'));
        %Upper Left
        SerialBitWriteCCU(Port,1,8,'0100',[11:-1:8],S1);pause(1)
        IupperLeft = getsnapshot(vid);
        
        %Upper Right
        SerialBitWriteCCU(Port,1,8,'0101',[11:-1:8],S1);pause(1)
        IupperRight = getsnapshot(vid);
        
        %Lower Left
        SerialBitWriteCCU(Port,1,8,'0110',[11:-1:8],S1);pause(1)
        IlowerLeft = getsnapshot(vid);
        
        %Lower Right
        SerialBitWriteCCU(Port,1,8,'0111',[11:-1:8],S1);pause(1)
        IlowerRight = getsnapshot(vid);
        
        %Reset Q3G mux
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);

        varargout{1} = [IupperLeft IupperRight; IlowerLeft IlowerRight];
        
    case 5 %8bit Upper Left
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0100',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
                
    case 6 %8bit Upper Right
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0101',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
        
    case 7 %8bit Lower Left
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0110',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);   
        
    case 8 %8bit Lower Right
        uiwait(msgbox('Don''t Use This Mode!'));
        SerialBitWriteCCU(Port,1,8,'0111',[11:-1:8],S1);
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,8,'0000',[11:-1:8],S1);
        
    case 9 %4k from Displayport
        %Far Left
        SerialBitWriteCCU(Port,1,4*256+8,'1000',[11:-1:8],S1);pause(1)
        FarLeft = DPunscramble(getsnapshot(vid));
        
        %Mid Left
        SerialBitWriteCCU(Port,1,4*256+8,'1001',[11:-1:8],S1);pause(1)
        MidLeft = DPunscramble(getsnapshot(vid));
        
        %Mid Right
        SerialBitWriteCCU(Port,1,4*256+8,'1010',[11:-1:8],S1);pause(1)
        MidRight = DPunscramble(getsnapshot(vid));
        
        %Far Right
        SerialBitWriteCCU(Port,1,4*256+8,'1011',[11:-1:8],S1);pause(1)
        FarRight = DPunscramble(getsnapshot(vid));
        
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        %Combine
        varargout{1} = cat(2,FarLeft,MidLeft,MidRight,FarRight);
                
    case 10 %Displayport Stripe 1
        SerialBitWriteCCU(Port,1,4*256+8,'1000',[11:-1:8],S1);pause(1)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 11 %Displayport Stripe 2
        SerialBitWriteCCU(Port,1,4*256+8,'1001',[11:-1:8],S1);pause(1)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 12 %Displayport Stripe 3
        SerialBitWriteCCU(Port,1,4*256+8,'1010',[11:-1:8],S1);pause(1)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 13 %Displayport Stripe 4
        SerialBitWriteCCU(Port,1,4*256+8,'1011',[11:-1:8],S1);pause(1)
        varargout{1} = DPunscramble(getsnapshot(vid));
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 14 %DisplayPort Downscaled to 1080p
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);pause(1)
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 15 %DisplayPort Downscaled to 1080p 12 bit
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);pause(1)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(1)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(1)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [Ileft Iright];
        
    case 16 %12bit 1920x1080 capture with Freeze Frame
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);
        
        %Freeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'1',[16],S1);
        
        %View Buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1',[15],S1);
        pause(0.2)
        %Disable Processing (Gamma and Post Peaking/Sharpening)
%         SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        SerialBitWriteCCU(Port,1,9,'1',0,S1);
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(0.2)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(0.2)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        
        %unFreeze Video
        SerialBitWriteCCU(Port,1,4*256+161,'0',[16],S1);
        
        %View Live Video
        SerialBitWriteCCU(Port,1,4*256+8,'0',[15],S1);
        
        varargout{1} = [Ileft Iright];
    
    case 17 %1080 from frame buffer
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(1)
        varargout{1} = getsnapshot(vid);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        
    case 18 %DisplayPort Downscaled to 1080p 12 bit
        SerialBitWriteCCU(Port,1,4*256+8,'1100',[11:-1:8],S1);pause(1)
        %12bit 1920x1080 capture  
        %Remeber Register 9
        reg9 = SerialReadCCU(Port,1,9,S1);

        %Disable Processing (Gamma and Post Peaking/Sharpening)
        SerialBitWriteCCU(Port,1,9,'11',[25:-1:24],S1);
        
        %Enable 12-bit mode left side
        SerialWriteCCU(Port,1,4*256+46,'00000001',S1);
        pause(1)
        Ic = getsnapshot(vid);
        Ileft = convert8to12(Ic);
        
        %Enable 12-bit mode right side
        SerialWriteCCU(Port,1,4*256+46,'000000F1',S1);
        pause(1)
        Ic = getsnapshot(vid);
        Iright = convert8to12(Ic);
        
        %Enable Processing
        SerialWriteCCU(Port,1,9,reg9,S1);
        
        %Disable 12-bit mode
        SerialWriteCCU(Port,1,4*256+46,'02940000',S1);
        SerialBitWriteCCU(Port,1,4*256+8,'0000',[11:-1:8],S1);
        varargout{1} = [Ileft Iright];
        
    case 19 %Display Port Capture Left no register changes
        varargout{1} = getsnapshot(vid);
end

if(length(varargin) >= 4)
    %Don't Close S1
else
    if(Mode ~= 1 && exist('S1','var'))
        fclose(S1);
    end
end

function out = DPunscramble(in)
% 0 0 0 0 0 0 | 1 1 1 1 1 1
% 2 2 2 2 2 2 | 3 3 3 3 3 3 ...

% 0 0 0 0 0 0
% 1 1 1 1 1 1 ...

out = zeros(2160,960,3);

for i = 1:3
    out(:,:,i) = reshape(in(:,:,i)',960,2160)';
end

out = uint8(out);
