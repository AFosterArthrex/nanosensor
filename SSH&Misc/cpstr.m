function [out] = cpstr(in,num)
%returns <num> copies of <in>
out = char(zeros(1,num));
for i = 1:num
    out(i) = in;
end