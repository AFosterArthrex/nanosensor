function [ status ] = RegWrite( Address, FPGA, Reg, Value, varargin )
%REGWRITE Writes 32-bit Binary Data to Register
%   Writes the 32-bit value to FPGA register using GPMC (Ethernet) or
%   RS232 (Serial)
%
%   Inputs:
%   1. Address - Serial Port (Serial Mode) or IP address (Ethernet)
%   2. FPGA - 1 = Formatter, 2 = Processor, 3 = Isolation
%   3. Reg - Target Register
%   4. Value - Value to Write
%   5. Op code (optional)
if(length(Address) > 3 && strcmpi(Address(1:3),'COM'))
    %Serial Mode
    answer = 'Retry';
    while(strcmpi(answer,'Retry') || strcmpi(answer,'Close All'))
        try
            SerialWriteCCU(Address,FPGA,Reg,Value,varargin{:});
            answer = 'Done';
        catch err
            getReport(err)
            if(strcmpi(err.identifier,'MATLAB:serial:fopen:opfailed'))
                answer = questdlg(sprintf('Cannot Open: %s.',Address),'Serial Communication Error','Retry','Close All','Cancel','Retry');
            end
            if(strcmpi('Close All',answer))
                ports = instrfindall
                fclose(ports(strcmpi(ports.Name,['Serial-' Port])))
                delete(ports(strcmpi(ports.Name,['Serial-' Port])))
            end
        end
    end
    status = 1;
else
    %Ethernet Mode
    if(length(varargin) >= 1)
        Op = varargin{1};
    else
        Op = 'o';
    end
    
    [s r] = SSHWriteCCU(Address,FPGA,Reg,Value);
    status = ~s;    
end

