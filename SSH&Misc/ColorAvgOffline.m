% function [r d] = ColorChart(handles)
%DSC Color Chart
C = zeros(10,3);

I = zeros(1080,1920,3,10);
I2 = zeros(1080,1920,3,10);

directory = uigetdir;
list = dir([directory '\*.png']);

labTransformation = makecform('srgb2lab');

for i = 1:length(list)
    i
    I = imread([directory '\' list(i).name] );
    loc = findColorBoxes(I);
    % I2(:,:,:,i) = rgb2ycbcr(I);
    % Iover = zeros(size(I),'double');
    Ilab = applycform(I,labTransformation);
    for j = 1:10
        
        M(j,:,i) = squeeze(mean(mean(I(loc(j,3):loc(j,4),loc(j,1):loc(j,2),:),1),2))';
        
        L(j,:,i) = squeeze(mean(mean(Ilab(loc(j,3):loc(j,4),loc(j,1):loc(j,2),:),1),2))';
        
        
        % E(i) = sum(sum(R- M(:,:,i)))/sum(sum(R));
        % Iover(loc(j,3)-1:loc(j,3)+1,loc(j,1):loc(j,2),2) = 1;
        % Iover(loc(j,4)-1:loc(j,4)+1,loc(j,1):loc(j,2),2) = 1;
        % Iover(loc(j,3):loc(j,4),loc(j,1)-1:loc(j,1)+1,2) = 1;
        % Iover(loc(j,3):loc(j,4),loc(j,2)-1:loc(j,2)+1,2) = 1;
    end
    % Iover = DrawBox2(Iover,loc);
    
end
fprintf('RGB Values (Gen1)\n')
fprintf('%014.10f %014.10f %014.10f\n',mean(M(:,:,:),3)')
fprintf('LAB Values (Gen2)\n')
fprintf('%014.10f %014.10f %014.10f\n',mean(L(:,:,:),3)')
Choices =   {'3MOS','HS3MOS','HD 1MOS','4k 1MOS','3CCD Integrated','3CCD CMount','Cancel'};
filenames = {'ColorValues - 3MOS.txt','ColorValues - HS3MOS.txt','ColorValues - 1MOS.txt','ColorValues - 4k1MOS.txt','ColorValues - 3CCDIntegrated.txt','ColorValues - 3CCDCMount.txt'};
A = bttnChoiseDialog(Choices,'',1,'Write Results to File?');

switch A
    case 1
        [FileName,PathName] = uiputfile(filenames{1});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(L(:,:,1:end),3)');
        fclose(fid);
    case 2
        [FileName,PathName] = uiputfile(filenames{2});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(L(:,:,1:end),3)');
        fclose(fid);
    case 3
        [FileName,PathName] = uiputfile(filenames{3});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(L(:,:,1:end),3)');
        fclose(fid);
    case 4
        [FileName,PathName] = uiputfile(filenames{4});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(L(:,:,1:end),3)');
        fclose(fid);
    case 5
        [FileName,PathName] = uiputfile(filenames{5});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(M(:,:,1:end),3)');
        fclose(fid);
    case 6
        [FileName,PathName] = uiputfile(filenames{6});
        fid = fopen([PathName FileName],'w');
        fprintf(fid,'%014.10f %014.10f %014.10f\r\n',mean(M(:,:,1:end),3)');
        fclose(fid);
    case 7
        %Cancel
    otherwise
        %Cancel
end