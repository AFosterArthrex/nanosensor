function [out] = convertraw(in)

fid = fopen(in); %open binary data file with name
input_image = fread(fid,[2958,548],'*uint16',0,'l')'; %read 16bit words into a matrix
size(input_image);
% extract the colors into a 3 plane format
out(:,:,1) = input_image(:,1:3:2956);  %red
out(:,:,2) = input_image(:,2:3:2957);  %green
out(:,:,3) = input_image(:,3:3:2958);  %blue
fclose(fid);
end