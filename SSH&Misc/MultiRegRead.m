function [ out ] = MultiRegRead( FPGA, Reg, varargin)

N = max([size(FPGA,1) size(Reg,1)]);

if(size(FPGA,1) == 1)
    for i = 2:N
        FPGA(i,:) = FPGA(1,:);
    end
end

for i = 1:N
    if(size(varargin,1))
        out(i,:) = FCORegRead( FPGA(i,:), Reg(i,:), varargin{1} );
    else
        out(i,:) = FCORegRead( FPGA(i,:), Reg(i,:) );
    end
end