function [ out ] = MultiRegWrite( FPGA, Reg, Data, varargin )

N = max([size(FPGA,1) size(Reg,1)  size(Data,1)]);

if(size(FPGA,1) == 1)
    for i = 2:N
        FPGA(i,:) = FPGA(1,:);
    end
end

if(size(Reg,1) == 1)
    for i = 2:N
        Reg(i,:) = Reg(1,:);
    end
end

if(size(Data,1) == 1)
    for i = 2:N
        Data(i,:) = Data(1,:);
    end
end

for i = 1:N
    if(size(varargin,1))
        out(i,:) = FCORegWrite( FPGA(i,:), Reg(i,:), Data(i,:), varargin{1} );
    else
        out(i,:) = FCORegWrite( FPGA(i,:), Reg(i,:), Data(i,:) );
    end
end