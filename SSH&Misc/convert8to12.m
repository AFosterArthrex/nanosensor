function out = convert8to12(I,varargin)
%UHD4 12 bit conversion
%Takes an 8 bit capture and outputs the 12bit original with 1/2 the
%horizontal resolution

if(~isempty(varargin))
    mode = varargin{1};
else
    mode = 'Gen2';
end

if(strcmpi(mode,'Gen1'))
    q = I;
    
    RedL = q(1:2:1079,1:2:1919,1);
    RedH = q(2:2:1080,1:2:1919,1);
    
    GreenL = q(1:2:1079,1:2:1919,2);
    GreenH = q(2:2:1080,1:2:1919,2);
    
    BlueL = q(1:2:1079,1:2:1919,3);
    BlueH = q(2:2:1080,1:2:1919,3);
    
    %Check image
    
%     mod(RedH,16) == floor(double(RedL)/16);
%     mod(GreenH,16) == floor(double(GreenL)/16);
%     mod(BlueH,16) == floor(double(BlueL)/16);
    
    Red = double(RedH)*16 + mod(double(RedL),16);
    Green = double(GreenH)*16 + mod(double(GreenL),16);
    Blue = double(BlueH)*16 + mod(double(BlueL),16);
    
    % Rescale Channels
    I = zeros(540,960,3,'uint16');
    I(:,:,1) = uint16(double(Red));%*4095/double(max(max(Red))));
    I(:,:,2) = uint16(double(Green));%*4095/double(max(max(Green))));
    I(:,:,3) = uint16(double(Blue));%*4095/double(max(max(Blue))));
    out = I;
else
    if(size(I,4) == 1)
        
        out = zeros(size(I,1),960,3);
        
        out = uint16(double(I(:,1:960,:))*2^4 + double(I(:,961:1920,:)));
        
    else
        
        out = zeros(size(I,1),960,3,size(I,4));
        
        out = uint16(double(I(:,1:960,:,:))*2^4 + double(I(:,961:1920,:,:)));
        
    end
end