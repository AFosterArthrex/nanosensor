function [gaussRing] = GaussianRing(R,sig,s)
gaussRing = zeros(s(2),s(1),'single');
[X,Y] = meshgrid(1:s(2),1:s(1));
gaussRing = exp(-((sqrt((Y-s(1)/2).^2+(X-s(2)/2).^2)-R).^2)./(2*sig^2));

gaussRing(gaussRing < 0.01) = 0;

gaussRing = single(gaussRing);
end