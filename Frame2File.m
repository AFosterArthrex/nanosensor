% Takes a frame's worth of data 924x288x14bits and writes it as a text file
% for modelsim

%data is 14-bits, stored as hex

load('S170404-3_Full_Sensor_Capture.mat','I')



hexNums = dec2hex(I);

hexCells = mat2cell(hexNums',4,ones(1,266112));

fid = fopen('Image.Data','w');

fprintf(fid,'%s\r\n',hexCells{:});

fclose(fid)